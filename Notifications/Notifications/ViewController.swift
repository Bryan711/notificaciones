//
//  ViewController.swift
//  Notifications
//
//  Created by BRYAN OCAÑA on 10/1/18.
//  Copyright © 2018 BRYAN OCAÑA. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var nombreUITextField: UITextField!
    @IBOutlet weak var datosNombreUILabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        //par recuperar
        //1. instnciar user defaults
        let deafult = UserDefaults.standard
        
        //2. acceder al valor por medio del key
        nombreUITextField.text = deafult.object(forKey: "label") as? String

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func GuardarButton(_ sender: Any) {
   
        datosNombreUILabel.text = nombreUITextField.text!

        //para guardar
        //1. instacniar user defaults
        let defaults = UserDefaults.standard
        
        //2. guardar variabls en defaults
        // int, bool, float, double, object
        
        defaults.set(nombreUITextField.text, forKey: "label")
        
    }
    
}

